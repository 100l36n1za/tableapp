import React, {useState, useEffect} from 'react';
import './App.css';
import Table from './components/Table';
import Search from './components/Search';
import ProfileCard from './components/ProfileCard';
import Pagination from './components/Pagination';
import AddEntryCard from './components/AddEntryCard';

function App() {

  const smallData = 'http://www.filltext.com/?rows=32&id={number|1000}&firstName={firstName}&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}'
  const bigData = 'http://www.filltext.com/?rows=1000&id={number|1000}&firstName={firstName}&delay=3&lastName={lastName}&email={email}&phone={phone|(xxx)xxx-xx-xx}&address={addressObject}&description={lorem|32}'

  const [dataType, setDataType] = useState(null)
  const [data, setData] = useState(null)
  const [searchQuery, setSearchQuery] = useState('')
  const [currentPage, setCurrentPage] = useState(1)
  const [currentEntries, setCurrentEntries] = useState(null)
  const [profileObject, setProfileObject] = useState(null)
  const [sortDirection, setSortDirection] = useState({id: 'asc', firstName: 'asc', lastName: 'asc', email: 'asc', phone: 'asc'})
  const [isFormOpen, setIsFormOpen] = useState(0)
  const [formError, setFormError] = useState(0)

  useEffect(() => {
    if(dataType){
      fetch(dataType.link)
      .then((response) => {
          return response.json();
        })
      .then((json) => {
          setData(json)
        })
      .catch((error) => {
        console.error(error);
      });
    }
  }, [dataType])
  

  useEffect(()=>{
      if(data){
          setCurrentEntries(search(data).slice(currentPage * 50 - 50, currentPage * 50))
      }
  }, [currentPage, data, searchQuery])

  function onDataTypeChange(e){
      e.preventDefault();
      setData(null)
      setCurrentEntries(0)
      if(dataType.text === 'меньше'){
          setDataType({text: 'больше', link: smallData});
      } else {
          setDataType({text: 'меньше', link: bigData});
      }
  }

  function search(someData){
      return someData.filter(element => {
          if(String(element.id).includes(searchQuery) || element.firstName.toLowerCase().includes(searchQuery) || element.lastName.toLowerCase().includes(searchQuery) || element.phone.includes(searchQuery)){
              return true
          } else {
              return false
          }
      })
  }

  function onSearch(input){
      if(searchQuery !== input){
          setSearchQuery(input)
      }
  }

  function sortBy(key){
      function compare(a, b) {
        if(sortDirection[key] === 'asc'){

            if(key === 'id'){
                return a[key] - b[key]
            } else if(key === 'phone') {
                return parseInt(a[key].split('').filter(el => '1234567890'.includes(el)).join('')) - parseInt(b[key].split('').filter(el => '1234567890'.includes(el)).join(''))
            } else {
                return a[key].localeCompare(b[key])
            }

        } else if(sortDirection[key] === 'desc') {

            if(key === 'id'){
                return b[key] - a[key]
            } else if(key === 'phone') {
                return parseInt(b[key].split('').filter(el => '1234567890'.includes(el)).join('')) - parseInt(a[key].split('').filter(el => '1234567890'.includes(el)).join(''))
            } else {
                return b[key].localeCompare(a[key])
            }

        }
      }

    
      setCurrentEntries(currentEntries.sort(compare))

      setSortDirection({...sortDirection, [key] : sortDirection[key] === 'asc' ? 'desc' : 'asc'})
  }

  function onProfileCardOpen(profile){
      setProfileObject(profile)
  }

  function onProfileCardClose(){
      setProfileObject(null)
  }

  function onPageChange(number){
      setCurrentPage(number)
  }

  function onNewEntryDataSend(newEntry){
      data.push(newEntry)
      setCurrentEntries(array_move(data, data.length - 1, 0).splice(currentPage * 50 - 50, currentPage * 50))
      onFormClosed()
  }

  function onFormClosed(){
      setIsFormOpen(0)
  }

  function onFormError(){
      setFormError(1)
  }

  function hideFormError(){
      setFormError(0)
  }
  
  function array_move(arr, old_index, new_index) {
    if (new_index >= arr.length) {
        var k = new_index - arr.length + 1;
        while (k--) {
            arr.push(undefined);
        }
    }
    arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
    return arr;
  };

  return (
    !dataType
    ? <div className="cntnr text-center pt-4">
          <h2>Выберите объем данных</h2>
          <div className="align-center">
              <button
                  type="button" 
                  className="btn btn-primary btn-lg mt-2 mx-2"
                  onClick={e => {e.preventDefault(); setDataType({text: 'больше', link: smallData})}}>Маленький</button>

              <button 
                  type="button"
                  className="btn btn-primary btn-lg mt-2 mx-2"
                  onClick={e => {e.preventDefault(); setDataType({text: 'меньше', link: bigData})}}>Большой</button>
          </div>
      </div> 
    : <div className="container">
          <div className="row">
              <div className="col-8">
                  <Search onSearch={onSearch}/>
                  {
                      currentEntries
                      ?
                      <Table data={currentEntries} sortBy={sortBy} onProfileCardOpen={onProfileCardOpen}/>
                      :
                      <h1 className="mt-3 ml-3">loading...</h1>
                  }
                  {   
                      (currentEntries && data.length > 50)
                      ?
                      <Pagination dataLength={search(data).length} entriesPerPage={50} onPageChange={onPageChange}/>
                      : null
                  }
              </div>
              <div className="col-4">
                    <button type="button" className="btn btn-primary btn-lg marginbtn" onClick={(e) => {e.preventDefault(); setIsFormOpen(1)}}>Добавить</button>
                    <button type="button" className="btn btn-secondary btn-lg marginbtn" onClick={(e) => {e.preventDefault(); onDataTypeChange(e)} }>Загрузить {dataType.text} данных</button>
                    {
                      profileObject
                      &&
                      <ProfileCard profileObject={profileObject} onProfileCardClose={onProfileCardClose} />
                    }
                    {
                        isFormOpen
                        ?
                        <AddEntryCard onNewEntryDataSend={onNewEntryDataSend} onFormClosed={onFormClosed} onFormError={onFormError}/>
                        : null
                    }
                    {
                        formError
                        ? <h1 className="mt-2 ml-2" onClick={(e) => {e.preventDefault(); hideFormError()}}>Все поля должны быть заполнены верно!</h1>
                        : null
                    }
              </div>
          </div>
      </div>
  )
}

export default App;
