import React from 'react';

function ProfileCard(props) {
    return(
      <div className="card border-dark mb-3">
            <div className="card-header font-weight-bold">
                {props.profileObject.firstName + ' ' + props.profileObject.lastName}
                <img src="https://cdn3.iconfinder.com/data/icons/ui-icons-5/16/cross-small-01-512.png" className="rounded float-right" width="20" height="20" alt="close card" onClick={(e) => { e.preventDefault(); props.onProfileCardClose()}}></img>
            </div>
            <div className="card-body text-dark">
                <p className="card-text font-weight-bold">Описание:</p>
                <p className="card-text card-text-value">{props.profileObject.description}</p>
                <p className="card-text font-weight-bold">Адрес проживания:</p>
                <p className="card-text card-text-value">{props.profileObject.address.streetAddress}</p>
                <p className="card-text font-weight-bold">Город:</p>
                <p className="card-text card-text-value">{props.profileObject.address.city}</p>
                <p className="card-text font-weight-bold">Штат:</p>
                <p className="card-text card-text-value">{props.profileObject.address.state}</p>
                <p className="card-text font-weight-bold">Индекс:</p>
                <p className="card-text card-text-value">{props.profileObject.address.zip}</p>
            </div>
      </div>
    )
}

export default ProfileCard;
