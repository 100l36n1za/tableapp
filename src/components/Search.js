import React from 'react';

function Search(props) {

  let inputValue = ''

  return(
    <form className="form-inline">
        <input className="form-control mr-sm-2 inpt" type="search" placeholder="Search" aria-label="Search" onChange={(e) => inputValue = e.target.value}/>
        <button className="btn btn-outline-success my-2 my-sm-0" type="submit" onClick={(e) => {e.preventDefault(); props.onSearch(inputValue)}}>Найти</button>
    </form>
  )

}

export default Search;
