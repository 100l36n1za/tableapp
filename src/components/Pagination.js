import React from 'react';

function Pagination(props) {
    
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(props.dataLength / props.entriesPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
      <nav>
        <ul className='pagination'>
          {pageNumbers.map((number, index) => 
            <li key={index} className='page-item page-link' onClick={(e) => {e.preventDefault(); props.onPageChange(number)}}>
            {number}
            </li>
          )}
        </ul>
    </nav>
  );
};

export default Pagination;