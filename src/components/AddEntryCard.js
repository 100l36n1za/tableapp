import React from 'react'

function AddEntryCard(props) {

    let newEntry = {address:{}}
    const reMail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const rePhone= /^\(\d{3}\)\d{3}\-\d{4}$/

    function checkForm(){
        if(newEntry.id && newEntry.lastName && newEntry.firstName && newEntry.email && newEntry.email.match(reMail) && newEntry.phone && newEntry.phone.match(rePhone) && newEntry.address && newEntry.description){
            props.onNewEntryDataSend(newEntry)
        } else {
            props.onFormError()
        }
    }

    return(
        <div className="card border-dark mb-3">
            <div className="card-header font-weight-bold">
                Добавление новой записи
                <img 
                    src="https://cdn3.iconfinder.com/data/icons/ui-icons-5/16/cross-small-01-512.png" 
                    className="rounded float-right" 
                    width="20" 
                    height="20" 
                    alt="close card" 
                    onClick={(e) => { e.preventDefault(); props.onFormClosed()}}/>
            </div>
            <div className="card-body text-dark">
                <p className="card-text font-weight-bold">ID:</p>
                <input
                    required    
                    className="form-control mr-sm-2 inpt" 
                    type="number" 
                    onChange={(e)=>{e.preventDefault(); newEntry.id = parseInt(e.target.value)}}/>

                <p className="card-text font-weight-bold">Имя:</p>
                <input 
                    required
                    className="form-control mr-sm-2 inpt" 
                    type="text" 
                    onChange={(e)=>{e.preventDefault(); newEntry.firstName = e.target.value}}/>

                <p className="card-text font-weight-bold">Фамилия:</p>
                <input
                    required
                    className="form-control mr-sm-2 inpt"
                    type="text" 
                    onChange={(e)=>{e.preventDefault(); newEntry.lastName = e.target.value}}/>

                <p className="card-text font-weight-bold">Электронная почта:</p>
                <input 
                    required
                    className="form-control mr-sm-2 inpt" 
                    type="email"
                    placeholder="at@example.com"
                    onChange={(e)=>{e.preventDefault(); newEntry.email = e.target.value}}/>

                <p className="card-text font-weight-bold">Телефон:</p>
                <input 
                    required
                    className="form-control mr-sm-2 inpt" 
                    type="email"
                    placeholder="(123)456-7890"
                    onChange={(e)=>{e.preventDefault(); newEntry.phone = e.target.value}}/>

                <p className="card-text font-weight-bold">Адрес проживания:</p>
                <input 
                    required
                    className="form-control mr-sm-2 inpt" 
                    type="text"
                    onChange={(e)=>{e.preventDefault(); newEntry.address.streetAddress = e.target.value}}/>

                <p className="card-text font-weight-bold">Город:</p>
                <input 
                    required
                    className="form-control mr-sm-2 inpt" 
                    type="text"
                    onChange={(e)=>{e.preventDefault(); newEntry.address.city = e.target.value}}/>

                <p className="card-text font-weight-bold">Штат:</p>
                <input 
                    required
                    className="form-control mr-sm-2 inpt" 
                    type="text"
                    onChange={(e)=>{e.preventDefault(); newEntry.address.state = e.target.value}}/>

                <p className="card-text font-weight-bold">Индекс:</p>
                <input 
                    required
                    className="form-control mr-sm-2 inpt" 
                    type="number"
                    onChange={(e)=>{e.preventDefault(); newEntry.address.zip = e.target.value}}/>

                <p className="card-text font-weight-bold">Описание:</p>
                <input 
                    required
                    className="form-control mr-sm-2 inpt" 
                    type="text"
                    onChange={(e)=>{e.preventDefault(); newEntry.description = e.target.value}}/>

                <button className="btn-dark px-3 py-2 mt-3 mb-2 rounded" onClick={(e)=> {e.preventDefault(); checkForm()}}>Добавить запись</button>
            </div>
      </div>
    )
}


export default AddEntryCard