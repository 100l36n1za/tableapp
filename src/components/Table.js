import React from 'react';

function Table(props) {

  return (

    <table className="table table-striped">
        <thead>
            <tr>
                <th onClick={(e) => {e.preventDefault(); props.sortBy(e.target.innerText)}}>id</th>
                <th onClick={(e) => {e.preventDefault(); props.sortBy(e.target.innerText)}}>firstName</th>
                <th onClick={(e) => {e.preventDefault(); props.sortBy(e.target.innerText)}}>lastName</th>
                <th onClick={(e) => {e.preventDefault(); props.sortBy(e.target.innerText)}}>email</th>
                <th onClick={(e) => {e.preventDefault(); props.sortBy(e.target.innerText)}}>phone</th>
            </tr>
        </thead>
        <tbody>
            {
                props.data.map( (el, index) =>
                    <tr key={index} onClick={(e) => {e.preventDefault(); props.onProfileCardOpen(props.data[index]);}}>
                        <td> {el.id} </td>
                        <td> {el.firstName} </td>
                        <td> {el.lastName} </td>
                        <td> {el.email} </td>
                        <td> {el.phone} </td>
                    </tr>
                )
            }
        </tbody>
    </table>

  )
}

export default Table;
